<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Mail\UserConfirmation;
use App\Role;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Notifications\VerifyEmail;
use Carbon\Carbon;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],

            'surname' => ['string', 'max:255'],
            'sa_id_number' => ['digits:13'],
            'mobile_number' => ['digits:10'],
            'date_of_birth' => ['date'],
            'language' => ['string'],
            'interests' => ['array'],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        return view('admin.users.create')->with([ 'interest_array' => $user->interest_array  ]);
    }

    private function getDOBfromID($said){
        //get the DOB from the IDnumber
        $date_of_birth_YY = substr($said, 0, 2);
        $date_of_birth_MM = substr($said, 2, 2);
        $date_of_birth_DD = substr($said, 4, 2);

        $now = date('Y'); //2019
        $nowmil = substr($now, 0, 2);
        $millenium = 19;
        if ($date_of_birth_YY < $nowmil) {
            $millenium = 20;
        }
        return $millenium . $date_of_birth_YY . '-' . $date_of_birth_MM . '-' . $date_of_birth_DD;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dob = $request['date_of_birth'];

        if (!empty($request['sa_id_number']) && empty($dob) ){
            $dob = $this->getDOBfromID($request['sa_id_number']);
        }

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),

            'surname' => $request['surname'],
            'sa_id_number' => $request['sa_id_number'],
            'mobile_number' => $request['mobile_number'],
            'date_of_birth' => $dob,
            'language' => $request['language'],
            'interests' => $request['interests']
        ]);

       // Mail::to($user)->send(new UserConfirmation($user));

        $details = ['email' => $request['email'] ];
        SendEmail::dispatch($details);

        return redirect(route('admin.users.index'))->with('success', 'User added!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //dd($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (Gate::denies('edit-users')) {
            return redirect(route('admin.users.index'));
        }
        $roles = Role::all();
        return view('admin.users.edit')->with([
                'user' => $user,
                'roles' => $roles
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //dd($request);
        $user->roles()->sync($request->roles);

        $dob = $request['date_of_birth'];

        if (!empty($request['sa_id_number']) && empty($dob) ){
            $dob = $this->getDOBfromID($request['sa_id_number']);
        }

        $user->name = $request['name'];
        $user->email = $request['email'];

        $user->surname = $request['surname'];
        $user->sa_id_number = $request['sa_id_number'];
        $user->mobile_number = $request['mobile_number'];
        $user->date_of_birth = $dob;
        $user->language = $request['language'];
        $user->interests = $request['interests'];

        if(!empty($request['password'])){
            $user->password = Hash::make($request['password']);
        }

        if($user->save()){
            $request->session()->flash('success', $user->name.' has been updated successfully.');
        } else {
            $request->session()->flash('error', 'There was an error updating '.$user->name);
        }

        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        //dd($user);
        if (Gate::denies('delete-users')) {
            return redirect(route('admin.users.index'));
        }
        $user->roles()->detach();
        $user->delete();

        return redirect()->route('admin.users.index');
    }
}
