<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $userRole = Role::where('name', 'user')->first();

        $user =  User::create([
            'name' => 'user',
            'surname' => 'surname',
            'email' => 'user@user.com',
            'password' => Hash::make('userpassword'),
        ]);

        $admin =  User::create([
            'name' => 'admin_user',
            'surname' => 'surname',
            'email' => 'admin@admin.com',
            'password' => Hash::make('adminpassword'),
        ]);

        $admin->roles()->attach($adminRole);
        $user->roles()->attach($userRole);
    }
}
