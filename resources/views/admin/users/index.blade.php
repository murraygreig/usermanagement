@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8">

                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if(session('warning'))
                    <div class="alert alert-warning">
                        {{ session('warning') }}
                    </div>
                @endif

                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> <h2>Users<a href="{{ route('admin.users.create') }}" class="btn btn-primary float-right">Create</a></div>
                    </h2>
                    <div class="card-body">

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Surname</th>
                                <th scope="col">Email</th>
                                <th scope="col">Roles</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th scope="row">{{ $user->name }}</th>
                                    <td>{{ $user->surname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ implode(',', $user->roles()->get()->pluck('name')->toArray()) }}</td>
                                    <td>
                                        @can('edit-users')
                                            <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-primary">Edit</a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can('delete-users')
                                            <form action="{{ route('admin.users.destroy', $user) }}" method="post" class="d-inline">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-warning">Delete</button>
                                            </form>
                                        @endcan
                                        </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <p>You should only be able to see this page if you are an admin</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
