@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit User: {{ $user->name }}</div>

                    <div class="card-body">
                        <form action="{{ route('admin.users.update', $user) }}" method="post" >
                            @csrf
                            {{ method_field('PUT') }}




                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="given-name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>

                                <div class="col-md-6">
                                    <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ $user->surname }}" required autocomplete="family-name" autofocus>

                                    @error('surname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sa_id_number" class="col-md-4 col-form-label text-md-right">{{ __('ID Number') }}</label>

                                <div class="col-md-6">
                                    <input id="sa_id_number" type="number" maxlength="13" class="form-control  @error('sa_id_number')  is-invalid @enderror" name="sa_id_number" value="{{ $user->sa_id_number }}" autocomplete="sa_id_number" autofocus>

                                    @error('sa_id_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mobile_number" class="col-md-4 col-form-label text-md-right">{{ __('Cell Number') }}</label>

                                <div class="col-md-6">
                                    <input id="mobile_number" type="tel" maxlength="10"  class="form-control @error('mobile_number') is-invalid @enderror" name="mobile_number" value="{{ $user->mobile_number }}" autocomplete="tel" autofocus>

                                    @error('mobile_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date_of_birth" class="col-md-4 col-form-label text-md-right">{{ __('Date of Birth') }}</label>

                                <div class="col-md-6">
                                    <input id="date_of_birth" type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" value="{{ date('Y-m-d', strtotime($user->date_of_birth))  }}"  autocomplete="date_of_birth" autofocus>

                                    @error('date_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="language" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>


                                <div class="col-md-6">

                                    <select id="language"
                                            class="form-control @error('language') is-invalid @enderror" name="language" >
                                        @foreach($user->language_array as $language)
                                            <option @if( isset($user->language) && $language == $user->language ) selected @endif>{{ $language }}</option>
                                        @endforeach
                                    </select>

                                    @error('language')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="interests" class="col-md-4 col-form-label text-md-right">{{ __('Interests') }}</label>

                                <div class="col-md-6">
                                    @foreach($user->interest_array as $interest)
                                        <div class="form-check">
                                            <label><input type="checkbox" name="interests[]" value="{{ $interest }}"
                                                          @if( isset($user->interests) && in_array($interest, $user->interests) ) checked @endif>
                                                {{ $interest }}</label>
                                        </div>
                                    @endforeach

                                    @error('interests')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Reset password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="roles" class="col-md-4 col-form-label text-md-right">{{ __('Roles') }}</label>
                                <div class="col-md-6">
                                    @foreach($roles as $role)
                                        <div class="form-check">
                                            <label><input type="checkbox" name="roles[]" value="{{ $role->id }}"
                                                          @if($user->roles->pluck('id')->contains($role->id)) checked @endif>
                                                {{ $role->name }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <a href="{{ route('admin.users.index') }}" class="btn  pull-right">Back</a>
                            <button type="submit" class="btn btn-primary float-right">Update</button>

                        </form>




                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
